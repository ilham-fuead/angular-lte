'use strict';

/**
 * @ngdoc overview
 * @name angularLteApp
 * @description
 * # angularLteApp
 *
 * Main module of the application.
 */
angular
  .module('angularLteApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ng-fastclick',
    'jkuri.slimscroll'
  ])
  .config(function ($locationProvider,$routeProvider) {
    $locationProvider.hashPrefix('');
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
