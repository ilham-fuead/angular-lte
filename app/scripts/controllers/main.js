'use strict';

/**
 * @ngdoc function
 * @name angularLteApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularLteApp
 */
angular.module('angularLteApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
