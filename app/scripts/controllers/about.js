'use strict';

/**
 * @ngdoc function
 * @name angularLteApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularLteApp
 */
angular.module('angularLteApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
